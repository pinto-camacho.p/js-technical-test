module.exports = class PalindromeFinder {

  constructor(dataSource) {
      this.dataSource = dataSource;
      this.palindromes = [];
      this.notPalindromes = [];
      this.getPalindromeLists(dataSource);
  }

  find() {
      return this.palindromes;
  }

  findOther() {
      return this.notPalindromes;
  }

  getPalindromeLists(dataSource)  {
      return dataSource.map(word => {
          if (word instanceof Array) {
              return this.getPalindromeLists(word);
          } else if (typeof(word) === "string") {
              this.isPalindrome(word) ? this.palindromes.push(word) : this.notPalindromes.push(word);
          } else {
              this.notPalindromes.push(word);
          }
      });
  }

  isPalindrome(word) {
      const palindrome = word.toLowerCase().split("").reverse().join("");
      return word === palindrome;
  }
}