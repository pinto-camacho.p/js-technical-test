/**
 *
 * You've been asked to implement a palindrome finder for the following DATA_SOURCE.
 * We want to handle strings values only.
 * Non string values should be ignored without crashing or reporting error
 *
 * > implement one or multiple methods to find palindromes in the provided source
 *
 *
 * EXECUTION ENVIRONMENT:
 * node >= 6.9.2
 *
 */

const DATA_SOURCE = [
    'ada',
    'alla',
    1234,
    1234321,
    ['barab'],
    'essayasse',
    'lala',
    'malayalam',
    'imNotAPalindromeObviously'
];
let outputPalindromes = [];
let outputNotPalindromes = [];

// ------------------------------------>
// please start your implementation below:
//

class PalindromeFinder {

    constructor(dataSource) {
        this.dataSource = dataSource;
        this.palindromes = [];
        this.notPalindromes = [];
        this.getPalindromeList(dataSource);
    }

    find() {
        return this.palindromes;
    }

    findOther() {
        return this.notPalindromes;
    }

    getPalindromeList(dataSource)  {
        return dataSource.map(word => {
            if (word instanceof Array) {
                return this.getPalindromeList(word);
            } else if (typeof(word) === "string") {
                this.isPalindrome(word) ? this.palindromes.push(word) : this.notPalindromes.push(word);
            } else {
                this.notPalindromes.push(word);
            }
        });
    }

    isPalindrome(word) {
        const palindrome = word.toLowerCase().split("").reverse().join("");
        return word === palindrome;
    }
}

const palindromeFinder = new PalindromeFinder(DATA_SOURCE);
outputPalindromes = palindromeFinder.find();
outputNotPalindromes = palindromeFinder.findOther();

//
// end of your implementation
// ----------------------------------- <

// should return words extracted from DATA_SOURCE that are palindromes:
console.log('found palindromes: ',palindromeFinder.find());

// should return words extracted from DATA_SOURCE that are not palindromes:
console.log('are not palindromes: ',palindromeFinder.findOther());

// should return words extracted from DATA_SOURCE  that are palindromes:
console.log('found palindromes: ',outputPalindromes);

// should return words extracted from DATA_SOURCE  that are not palindromes:
console.log('are not palindromes: ',outputNotPalindromes);

