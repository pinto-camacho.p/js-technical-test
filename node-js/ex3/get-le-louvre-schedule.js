/**
 *
 * Exercice 3 - organise you source code as you wish.
 *
 * EXECUTION ENVIRONMENT:
 * node >= 6.9.2
 *
 */
const fetchMuseums = require("./back/modules/FetchMuseums.js");
const daysOfWeek = require("./back/modules/enums/daysOfWeek.js");

const printLouvreOpeningHours = async () => {
  try {
    const ileDeFranceMuseumsJson = await fetchMuseums();
    const louvre = ileDeFranceMuseumsJson.find(museum => museum["nom_du_musee"] === "Musée du Louvre");
    console.log(getReadableFormat(louvre["periode_ouverture"]));
  } catch (err) {
    console.log(err);
  }
}

/**
 * Get the museum schedule in a print readable format
 * @return {String}
 */
getReadableFormat = (schedule) => {
  const scheduleTab = daysOfWeek.map(day => {
    return `${day} ${schedule[day]}`;
  });
  return scheduleTab.join("\n");
}


printLouvreOpeningHours();
