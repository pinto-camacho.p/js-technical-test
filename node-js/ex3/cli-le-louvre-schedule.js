const fs = require("fs");
const fetchMuseums = require("./back/modules/FetchMuseums.js");

const cli = async () => {
  const date = validateArgs(process.argv);
  if (date) {
    try {
      const schedule = await getLouvreSchedule();
      const week = Object.keys(schedule);
      const openingHoursStr = schedule[week[date.getDay()]];
      if (openingHoursStr === "Fermé") {
        console.log(openingHoursStr);
      } else {
        const currentHour = date.getHours();
        const openingHours = openingHoursStr.match(/\d{1,2}/g).map(hour => Number.parseInt(hour));
        if (currentHour < openingHours[0] || currentHour >= openingHours[1]) {
          console.log("Fermé");
        } else {
          console.log("Ouvert");
        }
      }
    } catch (err) {
      console.log(err);
    }
  } else {
    return;
  }
}

/**
 * Get Le musée du Louvre schedule
 * @return {Promise<any>} Le musée du Louvre Schedule
 */
const getLouvreSchedule = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("ile-de-france-museums.json", async (err, data) => {
      let museums;
      if (err) {
        if (err.code === "ENOENT") {
          try {
            museums = await fetchMuseums();
          } catch (err) {
            reject(err);
            return;
          }
        } else {
          reject(err);
          return;
        }
      } else {
        museums = JSON.parse(data);
      } 
      const louvre = museums.find(museum => museum["nom_du_musee"] === "Musée du Louvre");
      if (!louvre) reject("Le musée du Louvre not found");
      resolve(louvre["periode_ouverture"]);
    });
  });
}

/**
 * Validate the cli arguments
 * @param {Array} args Array of the cli arguments received
 * @return {Date|null} Returns a valide date or null
 */
const validateArgs = (args) => {
  if (args.length === 2) {
    return new Date();
  } else if (args.indexOf("-h") !== -1 || args.indexOf("--help") !== -1) {
    console.log("Usage: node cli-le-louvre-schedule.js [Options] [Date]\nOptions:\n -h, --help\tGet help\nDate:\n Accepted date format\t[YYYY-MM-DD]T[HH:MM:SS]\n Default is the current date");
    return null;
  } else {
    const date = new Date(args[2]);
    if (isNaN(Date.parse(date))) {
      console.log(`cli-le-louvre-schedule.js: invalid arguments or option: ${args[2]}\nHelp: -h, --help`);
    } else {
      return date;
    }
  }
}

cli();