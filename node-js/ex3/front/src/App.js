import React from "react";

export function App(props) {
  return (
    <div>
      <h1>Période d'ouverture du Musée du Louvre</h1>
      <div>{props.data}</div>
    </div>
  );
}