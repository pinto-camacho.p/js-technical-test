const path = require("path");
const fs = require( "fs");
const React = require( "react");
const express = require( "express");
const ReactDOMServer = require( "react-dom/server");

const fetchMuseums = require("./modules/FetchMuseums.js");
const { App } = require( "../front/src/App.js");

const PORT = 3000;

const app = express();

app.get('/', async (req, res) => {
  const museums = await fetchMuseums();
  const louvre = museums.find(museum => museum["nom_du_musee"] === "Musée du Louvre");
  const app = ReactDOMServer.renderToString(<App data={louvre["periode_ouverture"]}/>);

  fs.readFile("./dist/index.html", (err, data) => {
    if (err) {
      console.log("Something went wrong", err);
      res.status(500).send("Something went wrong");
    }
    res.status(200).send(
      data.toString().replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.listen(PORT, () =>{
  console.log(`Server running on port ${PORT}`);
});
