const daysOfWeek = require("./enums/daysOfWeek.js");

/** Class representing a museum openning schedule */
class MuseumScheduler {
  /**
   * Create a museum schedule
   * @param {Object} museum
   */
  constructor(museum) {
    this.schedule = {};  
    this.#forgeSchedule(museum);
  }

  /**
   * Get the museum schedule
   * @return {Object} The museum schedule object
   */
  getSchedule = () => {
    return this.schedule;
  }

  /**
   * Parse the museum openning hours to a map {day: "hours"}
   * @param {Object} museum
   */
  #forgeSchedule = (museum) => {
    if (!museum["periode_ouverture"]) {
      return;
    }
    const openingStr = museum["periode_ouverture"].toLowerCase();
    const hoursTab = openingStr.match(/\d{1,2}h\sà\s\d{1,2}h/g);
    
    let startIndex = 0;
    const indexTab = hoursTab.reduce((acc, hour) => {
      const endIndex = openingStr.indexOf(hour);
      acc.push(openingStr.slice(startIndex, endIndex));
      acc.push(hour);
      startIndex = endIndex + hour.length;
      return acc;
    }, []);
    
    daysOfWeek.forEach(day => {
      const dayIndex = indexTab.findIndex(elt => elt.includes(day));
      if (dayIndex !== -1) {
        this.schedule[day] = indexTab[dayIndex + 1];
      } else {
        this.schedule[day] = "Fermé"
      }
    });
  }
}

module.exports = MuseumScheduler;