const axios = require('axios');
const xlsx = require("xlsx");
const Utils = require("./Utils.js");
const MuseumScheduler = require("./MuseumScheduler.js");

const endpointUrl = "https://www.data.gouv.fr/api/1/datasets/53699934a3a729239d2051a1/";
const keysToChange = [
  "NOM DU MUSEE",
  "ADR",
  "VILLE",
  "CP",
  "SITWEB",
  "PERIODE OUVERTURE"
];

/**
 * Get all Ile-de-France museums
 * @return {Array<Object>} Array of the Ile-de-France museums
 */
const fecthMuseums = async () => {
  const { data } = await axios.get(endpointUrl);
  
  const odsFiles = data.resources.filter(file => file.format === "ods");
  const lastModifiedList = odsFiles.map(file => Date.parse(file.last_modified));
  const mostRecentDateIndex = lastModifiedList.indexOf(Math.max(...lastModifiedList));
  const mostRecentFile = odsFiles[mostRecentDateIndex];
  
  await Utils.downloadFile(mostRecentFile.url, "museum.ods");
  
  const workbook = xlsx.readFile("museum.ods");
  const wbJson = xlsx.utils.sheet_to_json(workbook.Sheets["R_Coordonnées musées de France"]);
  const ileDeFranceMuseums = wbJson.filter(museum => museum["NOMREG"] === "ILE-DE-FRANCE");
  
  const ileDeFranceMuseumsJson = ileDeFranceMuseums.map((museum) => Utils.renameObjKeys(keysToChange, museum, (key) => key.toLowerCase().split(" ").join("_")));
  // const louvreIndex = ileDeFranceMuseumsJson.findIndex(museum => museum["nom_du_musee"] === "Musée du Louvre");
  // const louvreScheduler = new MuseumScheduler(ileDeFranceMuseumsJson[louvreIndex]);
  // ileDeFranceMuseumsJson[louvreIndex]["periode_ouverture"] = louvreScheduler.getSchedule();

  await Utils.saveJsonFile("ile-de-france-museums.json", ileDeFranceMuseumsJson);

  return ileDeFranceMuseumsJson;
}

module.exports = fecthMuseums;