const utils = require("../Utils.js");

describe("The rename keys function", () => {
  test('rename {"PERIODE OUVERTURE": "lundi"} keys to equal {"periode_ouverture" : "lundi"}', () => {
    const data = {"PERIODE OUVERTURE" : "lundi"};
    const transformFunc = (key) => key.toLowerCase().split(" ").join("_");
    const result = utils.renameObjKeys(["PERIODE OUVERTURE"], data, transformFunc);
    expect(result).toEqual({"periode_ouverture" : "lundi"});
  });
  
  test('rename {"NOMDEP": "Essonne"} keys to equal {"nomdep" : "Essonne"} with key without spaces', () => {
    const data = {"NOMDEP" : "Essonne"};
    const transformFunc = (key) => key.toLowerCase().split(" ").join("_");
    const result = utils.renameObjKeys(["NOMDEP"], data, transformFunc);
    expect(result).toEqual({"nomdep" : "Essonne"});
  });
  
  test('rename {"SITEWEB": "www.site.fr"} keys to equal {"SITEWEB" : "www.site.fr"} with key not included in array', () => {
    const data = {"SITEWEB": "www.site.fr"};
    const transformFunc = (key) => key.toLowerCase().split(" ").join("_");
    const result = utils.renameObjKeys(["NOMDEP"], data, transformFunc);
    expect(result).toEqual({"SITEWEB": "www.site.fr"});
  });
  
  test('rename {"SITEWEB": "www.site.fr"} keys to equal {"SITEWEB" : "www.site.fr"} with empty array', () => {
    const data = {"SITEWEB": "www.site.fr"};
    const transformFunc = (key) => key.toLowerCase().split(" ").join("_");
    const result = utils.renameObjKeys([], data, transformFunc);
    expect(result).toEqual({"SITEWEB": "www.site.fr"});
  });
});
