const MusuemScheduler = require("../MuseumScheduler.js");

const museum = {
  "NOMREG": "ILE-DE-FRANCE",
  "NOMDEP": "PARIS",
  "nom_du_musee": "Etablissement Public du Musée d'Orsay",
  "adr": "62, Rue de Lille",
  "cp": "75007",
  "ville": "PARIS",
  "TELEPHONE1": "0140494814",
  "FAX": "0145485660",
  "sitweb": "www.musee-orsay.fr",
  "FERMETURE ANNUELLE": "Jours fériés",
  "periode_ouverture": "Ouvert le mardi, mercredi, vendredi, dimanche de 9h à 18h, le jeudi de 9h à 21h",
  "JOURS NOCTURNES": "Jeudi jusqu'à 21h45"
};

describe("The Musée d'Orsay", () => {
  test("Generate schedule for Musée d'Orsay", () => {
    const orsayScheduler = new MusuemScheduler(museum);
    const expectedSchedule = {
      "dimanche": "9h à 18h",
      "lundi": "Fermé",
      "mardi": "9h à 18h",
      "mercredi": "9h à 18h",
      "jeudi": "9h à 21h",
      "vendredi": "9h à 18h",
      "samedi": "Fermé"
    };
    expect(orsayScheduler.getSchedule()).toEqual(expectedSchedule);
  });
});