const fs = require("fs");
const axios = require("axios");

class Utils {

  /** 
   * Download and save a file from the url
   * @param {String} url The url of the file to download.
   * @param {String} path The path where to save the downloaded file.
   */
  static downloadFile = async (url, path) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await axios({
          method: "GET",
          url,
          responseType: "stream"
        });
        response.data.pipe(fs.createWriteStream(path));
        response.data.on("error", (err) => {
          reject({message: `Writing ${path} file failed`, stack: err});
        });
        response.data.on("end", () => {
          resolve();
        });
      } catch (err) {
        reject({message: "GET file failed", stack: err});
      }
    });
  }

  /** 
   * Save a JSON Object at the path
   * @param {String} path The path of the JSON file.
   * @param {Object} data The data to save in the file.
   */
  static saveJsonFile = (path, data) => {
    return new Promise((resolve, reject) => {
      fs.writeFile(path, JSON.stringify(data, null, 2), (err) => {
        if (err) reject({message: "Writing JSON file failed", stack: err});
        resolve();
      });
    });
  }

  /**
   * Rename keys of an object
   * @param {Array<String>} keysToChange An Array of keys to transform in the object.
   * @param {Object} obj The object on which the transformation is taking place.
   * @param {Function} transform The transformation function to apply to each keys in the array.
   * @return {Object} The object with the transformed keys. 
   */
  static renameObjKeys = (keysToChange, obj, transform) => {
    const keysMap = keysToChange.reduce((map, key) => {
      map[key] = transform(key);
      return map;
    }, {});
    return Object.keys(obj).reduce((acc, key) => {
      return {
        ...acc,
        [keysMap[key] || key]: obj[key]
      }
    }, {});
  }
}

module.exports = Utils;